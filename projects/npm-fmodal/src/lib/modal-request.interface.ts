import { Type } from '@angular/core';
import { AModal } from '../modal-wrapper/modal-abstract';

export interface ModalRequest {
  type: Type<AModal>;
  modalData: any;
}