import { Injectable, Type } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { AModal } from './modal-abstract';
import { ModalRequest } from './modal-request.interface';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private nextModalDataSource = new BehaviorSubject<ModalRequest>(null);
  public nextModalData = this.nextModalDataSource.asObservable();
  public answerData = new Subject<any>();

  constructor() { }

  public openModal(type: Type<AModal>, modalData: any): Subject<any> {
    this.nextModalDataSource.next({
      type,
      modalData
    } as ModalRequest);

    this.answerData = new Subject<any>();

    return this.answerData;
  }

  public onAnswerReceived(answer: any): void {
    this.answerData.next(answer);
  }

  public onComplete(): void {
    this.answerData.complete();
  }
}
