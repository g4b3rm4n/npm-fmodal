/*
 * Public API Surface of npm-fmodal
 */

export * from './lib/npm-fmodal.service';
export * from './lib/npm-fmodal.component';
export * from './lib/npm-fmodal.module';
